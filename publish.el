;;; publish.el --- Ody55eus.gitlab.io Webpage -*- lexical-binding: t; -*-

;; Copyright (C) 2018 Pierre Neidhardt <mail@ambrevar.xyz>
;; Copyright (C) 2021 David Wilson <david@daviwil.com>
;; Copyright (C) 2022/2024 Jonathan Pieper <jpieper@mailbox.org>
;
;; Author: Jonathan Pieper <https://gitlab.com/ody55eus>
;; Maintainer: Jonathan Pieper <ody55eus@mailbox.org>
;; URL: https://ody55eus.de
;; Created: September 22, 2021
;; Modified: 23. November 2024
;; Version: 0.0.3
;; Keywords: org roam publish html css
;; Homepage: https://github.com/ody55eus/ody55eus.gitlab.io/
;; Package-Requires: ((emacs "29.4"))
;;
;; This file is not part of GNU Emacs.
;;

;; This file is loosely based on David Wilson's publish.el, here's his
;; authorship details:

;; Author: David Wilson <david@daviwil.com>
;; Maintainer: David Wilson <david@daviwil.com>
;; URL: https://sr.ht/~systemcrafters/site
;; Version: 0.0.1
;; Package-Requires: ((emacs "26.1"))
;; Keywords: hypermedia, blog, feed, rss

;; This file is loosely based on Pierre Neidhardt's publish.el, here's his
;; authorship details:

;; Author: Pierre Neidhardt <mail@ambrevar.xyz>
;; Maintainer: Pierre Neidhardt <mail@ambrevar.xyz>
;; URL: https://gitlab.com/Ambrevar/ambrevar.gitlab.io

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Docs License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Docs License for more details.
;;
;; You should have received a copy of the GNU General Docs License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Usage:
;; emacs -Q --batch -l ./publish.el --funcall dw/publish

(require 'ox-publish)
(require 'ox-html)
(require 'org-roam)
(require 'find-lisp)
(require 'htmlize)

(defun jp/init-webpage ()
  (defvar jp/url "https://ody5.eu")
  (defvar jp/gl-url "https://gitlab.ody5.de")
  (defvar jp/repository "https://gitlab.com/ody55eus/digital-garden")
  (defvar jp/root (expand-file-name "."))
  (defvar jp/zk-root (expand-file-name "digital-garden" jp/root))
  (eval-after-load 'org 
    (org-babel-do-load-languages
       'org-babel-load-languages
        '((shell . t))))
  (setq org-roam-directory (concat
                            jp/root
                            "/digital-garden/")
        org-confirm-babel-evaluate nil
        org-link-abbrev-alist '(("gitlab" . "https://gitlab.com/")
                                ("github" . "https://github.com/")
                                ("ody5" . (concat jp/gl-url "/"))
                                )
        org-directory (concat
                       jp/root
                       "/digital-garden/")
        org-id-locations-file (concat jp/root "/.org-id-locations")
        org-roam-db-location (concat jp/root "/org-roam.db")
        org-id-extra-files (find-lisp-find-files org-roam-directory "\.org$")
        org-roam-capture-templates '(("d" "default" plain
                                      "%?\n\nSee also %a.\n"
                                      :if-new (file+head
                                               "${slug}.org"
                                               "#+title: ${title}\n")
                                      :unnarrowed t)
                                     ("b" "Blog Entry" plain
                                      "%?"
                                      :if-new (file+head
                                               "Blog/${slug}.org"
                                               "#+title: ${title}\n#+date: %U")
                                      :unnarrowed t
                                      )
                                     ("l" "Literature")
                                     ("ll" "Literature Note" plain
                                      "%?\n\nSee also %a.\n* Links\n- %x\n* Notes\n"
                                      :if-new (file+head
                                               "Literature/${slug}.org"
                                               "#+title: ${title}\n")
                                      :unnarrowed t
                                      )
                                     ("lr" "Bibliography reference" plain
                                      "#+ROAM_KEY: %^{citekey}\n#+PROPERTY: type %^{entry-type}\n#+FILETAGS: %^{keywords}\n#+AUTHOR: %^{author}\n%?"
                                      :if-new (file+head
                                               "References/${citekey}.org"
                                               "#+title: ${title}\n")
                                      :unnarrowed t
                                      )
                                     ("p" "Projects" plain
                                      "%?"
                                      :if-new (file+head
                                               "Projects/${slug}.org"
                                               "#+title: ${title}\n")
                                      :unnarrowed t
                                      )
                                     )))
(jp/init-webpage)

;; Timestamps can be used to avoid rebuilding everything.
;; This is useful locally for testing.
;; It won't work on Gitlab when stored in ./: the timestamps file should
;; probably be put inside the public/ directory.  It's not so useful there
;; however since generation is fast enough.
(setq org-publish-use-timestamps-flag t
      org-publish-timestamp-directory "./")

;; Get rid of index.html~ and the like that pop up during generation.
(setq make-backup-files nil)

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-email t
      org-export-with-date t
      org-export-with-tags 'not-in-toc
      org-export-with-toc t
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-style org-html-style-default
      org-html-htmlize-output-type 'css
      org-html-doctype "html5")

(defun jp/preamble (info)
  "Return preamble as a string."
  (let* ((file (plist-get info :input-file))
         (gitlab-url "https://gitlab.com/ody55eus")
         (github-url "https://github.com/ody55eus")
         (prefix (file-relative-name jp/zk-root
                                     (file-name-directory file)))
         (prefix2 (file-relative-name
                                     (file-name-directory file)
                                     jp/zk-root))
         (filename (file-name-nondirectory file))
         (filepath (if (string-equal prefix2 ".")
                           filename
                           (concat prefix2 filename))))
    (format
     (concat
     "
<img class=\"logo\" src=\"%1$s/logo.png\">
<md-filled-button id='usage-anchor' href=\"%1$s/index.html\"><i class=\"fa fa-home\"></i> About</md-filled-button>
<md-filled-button slot='headline' href=\"%1$s/Projects/projects.html\"><i class=\"fa fa-folder-open\"></i> Projects</md-filled-button>
<md-filled-tonal-button href=\"%4$s\"><i class=\"fa fa-gitlab\"></i> GitLab</md-filled-tonal-button>
<md-filled-tonal-button href=\"%5$s\"><i class=\"fa fa-github\"></i> GitHub</md-filled-tonal-button>
<span class=\"source\"><md-elevated-button href=\"%3$s/-/tree/main/%2$s\"><i class=\"fa fa-code\"></i> Page-Source</md-elevated-button></span>
")
     prefix filepath jp/repository gitlab-url github-url)))

 ;; Use custom preamble function to compute relative links.
 org-html-preamble #'jp/preamble

(setq ;; org-html-divs '((preamble "header" "top")
 ;;                 (content "main" "content")
 ;;                 (postamble "footer" "postamble"))
 org-html-postamble t
 org-html-postamble-format `(("en" ,(concat "<p class=\"comments\"><a href=\""
                                            jp/repository "/issues\">Comments</a></p>

<p class=\"date\">Date: %u</p>
<p class=\"creator\">Made with %c</p>
<p class=\"license\">
  &copy; 2021-2024 Jonathan Pieper
  <a rel=\"license\" href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\"><img alt=\"GNU General Public License\" width=\"64px\" style=\"border-width:0\" src=\"https://www.gnu.org/graphics/gplv3-127x51.png\" /></a>
  <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Creative Commons License\" width=\"64px\" style=\"border-width:0\" src=\"https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png\" /></a>
</p>")))
)

;; Some help functions
(defun jp/git-creation-date (file)
  "Return the first commit date of FILE.
Format is %Y-%m-%d."
  (with-temp-buffer
    (call-process "git" nil t nil "log" "--reverse" "--date=short" "--pretty=format:%cd" file)
    (goto-char (point-min))
    (buffer-substring-no-properties (line-beginning-position) (line-end-position))))

(defun jp/git-last-update-date (file)
  "Return the last commit date of FILE.
Format is %Y-%m-%d."
  (with-output-to-string
    (with-current-buffer standard-output
      (call-process "git" nil t nil "log" "-1" "--date=short" "--pretty=format:%cd" file))))

(defun jp/org-html-format-spec (info)
  "Return format specification for preamble and postamble.
INFO is a plist used as a communication channel.
Just like `org-html-format-spec' but uses git to return creation and last update
dates.
The extra `u` specifier displays the creation date along with the last update
date only if they differ."
  (let* ((timestamp-format (plist-get info :html-metadata-timestamp-format))
         (file (plist-get info :input-file))
         (meta-date (org-export-data (org-export-get-date info timestamp-format)
                                     info))
         (creation-date (if (string= "" meta-date)
                            (jp/git-creation-date file)
                          ;; Default to the #+DATE value when specified.  This
                          ;; can be useful, for instance, when Git gets the file
                          ;; creation date wrong if the file was renamed.
                          meta-date))
         (last-update-date (jp/git-last-update-date file)))
    `((?t . ,(org-export-data (plist-get info :title) info))
      (?s . ,(org-export-data (plist-get info :subtitle) info))
      (?d . ,creation-date)
      (?T . ,(format-time-string timestamp-format))
      (?a . ,(org-export-data (plist-get info :author) info))
      (?e . ,(mapconcat
	      (lambda (e) (format "<a href=\"mailto:%s\">%s</a>" e e))
	      (split-string (plist-get info :email)  ",+ *")
	      ", "))
      (?c . ,(plist-get info :creator))
      (?C . ,last-update-date)
      (?v . ,(or (plist-get info :html-validation-link) ""))
      (?u . ,(if (string= creation-date last-update-date)
                 creation-date
               (format "%s (<a href=%s>Last update: %s</a>)"
                       creation-date
                       (format "%s/commits/main/%s" jp/repository (file-relative-name file jp/zk-root))
                       last-update-date))))))
(advice-add 'org-html-format-spec :override 'jp/org-html-format-spec)

(defun jp/org-publish-find-date (file project)
  "Find the date of FILE in PROJECT.
Just like `org-publish-find-date' but do not fall back on file
system timestamp and return nil instead."
  (let ((file (org-publish--expand-file-name file project)))
    (or (org-publish-cache-get-file-property file :date nil t)
    (org-publish-cache-set-file-property
     file :date
     (let ((date (org-publish-find-property file :date project)))
       ;; DATE is a secondary string.  If it contains
       ;; a time-stamp, convert it to internal format.
       ;; Otherwise, use FILE modification time.
           (let ((ts (and (consp date) (assq 'timestamp date))))
         (and ts
          (let ((value (org-element-interpret-data ts)))
            (and (org-string-nw-p value)
             (org-time-string-to-time value))))))))))

(defun jp/include-theme (&optional relative)
  "Includes theme RELATIVE to current path."
  (concat "<link rel=\"stylesheet\" type=\"text/css\" href=\"" (when relative "../") "css/dark.css\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"" (when relative "../") "css/htmlize.css\">
<link rel=\"icon\" type=\"image/x-icon\" href=\"" (when relative "../") "logo.png\">
<script type=\"module\" src=\"" (when relative "../") "js/index.js\"></script>
<script src=\"" (when relative "../") "js/bundle.js\"></script>
<script type=\"importmap\">
    {
      \"imports\": {
        \"@material/web/\": \"https://esm.run/@material/web/\"
      }
    }
  </script>
  <script type=\"module\">
    import '@material/web/all.js';
    import {styles as typescaleStyles} from '@material/web/typography/md-typescale-styles.js';

    document.adoptedStyleSheets.push(typescaleStyles.styleSheet);
  </script>"))

(defun jp/org-publish-sitemap (title list)
  "Outputs site map, as a string.
See `org-publish-sitemap-default'. "
  ;; Remove index and non articles.
  (setcdr list (seq-filter
                (lambda (file)
                  (string-match "file:.*.org" (car file)))
                (cdr list)))
  ;; TODO: Include subtitle?  It may be wiser, at least for projects.
  (concat "#+TITLE: " title "\n"
          "#+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/dark.css\" />"
          "\n"
          "#+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/htmlize.css\">"
          "\n"
          "#+HTML_HEAD: <link rel=\"icon\" type=\"image/x-icon\" href=\"../logo.png\"> "
          "\n"
          "#+HTML_HEAD: <script src=\"../js/bundle.js\"></script>"
          "\n"
          (org-list-to-org list)))

(defun jp/org-publish-main-sitemap (title list)
  "Outputs site map, as a string.
See `org-publish-sitemap-default'. "
  ;; Remove index and non articles.
  (setcdr list (seq-filter
                (lambda (file)
                  (string-match "file:.*.org" (car file)))
                (cdr list)))
  (concat "#+TITLE: " title "\n"
          "#+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"css/dark.css\" />"
          "\n"
          "#+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"css/htmlize.css\">"
          "\n"
          "#+HTML_HEAD: <link rel=\"icon\" type=\"image/x-icon\" href=\"logo.png\"> "
          "\n"
          "#+HTML_HEAD: <script src=\"js/bundle.js\"></script>"
          "\n"
          (org-list-to-org list)))

(defun jp/org-publish-sitemap-entry (entry style project)
  "Custom format for site map ENTRY, as a string.
See `org-publish-sitemap-default-entry'."
  (cond ((not (directory-name-p entry))
         (let* ((meta-date (jp/org-publish-find-date entry project))
                (file (expand-file-name entry
                                        (org-publish-property :base-directory project)))
                (creation-date (if (not meta-date)
                                   (jp/git-creation-date file)
                                 ;; Default to the #+DATE value when specified.  This
                                 ;; can be useful, for instance, when Git gets the file
                                 ;; creation date wrong if the file was renamed.
                                 (format-time-string "%Y-%m-%d" meta-date)))
                (last-date (jp/git-last-update-date file)))
           (format "[[file:%s][%s]]^{ (%s)}"
                   entry
                   (org-publish-find-title entry project)
                   (if (string= creation-date last-date)
                       creation-date
                     (format "%s, updated %s" creation-date last-date)))))
	((eq style 'tree)
	 ;; Return only last subdir.
	 (capitalize (file-name-nondirectory (directory-file-name entry))))
	(t entry)))

(setq org-publish-project-alist
      (list
       (list "all-site-pages-org"
             :base-directory "./digital-garden/"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public/"
             :with-broken-links t
             :auto-sitemap nil
             :html-preamble #'jp/preamble
             :html-head-include-default-style nil
             :html-head-include-scripts nil
             :html-head (jp/include-theme))
       (list "main-site-org"
             :base-directory "./digital-garden/"
             :recursive nil
             :html-preamble #'jp/preamble
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public/"
             :sitemap-format-entry #'jp/org-publish-sitemap-entry
             :with-broken-links t
             :auto-sitemap t
             :sitemap-title "Sitemap"
             :sitemap-filename "sitemap.org"
             ;; :sitemap-file-entry-format "%d *%t*"
             :sitemap-style 'list
             :sitemap-function #'jp/org-publish-main-sitemap
             ;; :sitemap-ignore-case t
             :sitemap-sort-files 'anti-chronologically
             :html-head-include-default-style nil
             :html-head-include-scripts nil
             :html-head (jp/include-theme))
       (list "site-projects-org"
             :base-directory "./digital-garden/Projects"
             :recursive t
             :html-preamble #'jp/preamble
             :with-broken-links t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public/Projects"
             :sitemap-format-entry #'jp/org-publish-sitemap-entry
             :auto-sitemap t
             :sitemap-title "Projects"
             :sitemap-filename "projects.org"
             ;; :sitemap-file-entry-format "%d *%t*"
             :sitemap-style 'list
             :sitemap-function #'jp/org-publish-sitemap
             ;; :sitemap-ignore-case t
             :sitemap-sort-files 'anti-chronologically
             :html-head-include-default-style nil
             :html-head-include-scripts nil
             :html-head (jp/include-theme t))
       (list "site-literature-org"
             :base-directory "./digital-garden/Literature"
             :recursive nil
             :html-preamble #'jp/preamble
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public/Literature"
             :auto-sitemap nil
             :html-head-include-default-style nil
             :html-head-include-scripts nil
             :html-head (jp/include-theme t))
       (list "site-static"
             :base-directory "static/"
             :exclude "\\.org\\'"
             :base-extension 'any
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("main-site-org" "site-projects-org" "site-literature-org" "site-static"))))

(defun jp/publish-html ()
  (jp/init-webpage)
  (org-roam-setup)
  (org-id-update-id-locations)
  (org-publish-all)
  )

(provide 'publish)
;;; publish.el ends here
