;; This "manifest" file can be passed to 'guix package -m' to reproduce
;; the content of your profile.  This is "symbolic": it only specifies
;; package names.  To reproduce the exact same profile, you also need to
;; capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(specifications->manifest
  (list "emacs"
        "emacs-org"
        "emacs-org-contrib"
        "emacs-org-roam"
        "emacs-org-ref"
        "emacs-org-roam-bibtex"
        "emacs-esxml"
        "emacs-webfeeder"
        "emacs-compat"
        "emacs-emacsql"
        "emacs-ox-pandoc"
        "emacs-htmlize"
        "emacs-sqlite3-api"
        "git"
        "make"
        "node"
        "imagemagick"))
